import React, { Component } from "react";
import { Link, withRouter } from "react-router-dom";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { registerUser } from "../../actions/authActions";
import classnames from "classnames";

class Register extends Component {
  constructor() {
    super();
    this.state = {
      name: "",
      email: "",
      password: "",
      password2: "",
      // imageName: "",
      image:"",
      // imageData:"",
      errors: {},
    };
  }

  componentDidMount() {
    // If logged in and user navigates to Register page, should redirect them to dashboard
    if (this.props.auth.isAuthenticated) {
      this.props.history.push("/dashboard");
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.errors) {
      this.setState({
        errors: nextProps.errors,
      });
    }
  }

  onChange = (e) => {
    this.setState({ [e.target.id]: e.target.value });
  };

  onSubmit = (e) => {
    e.preventDefault();

    const newUser = {
      name: this.state.name,
      email: this.state.email,
      password: this.state.password,
      password2: this.state.password2,
      // imageName: this.state.imageName,
      image:this.state.image,
      // imageData:this.state.imageData
    };

    this.props.registerUser(newUser, this.props.history);
  };

  render() {
    const { errors } = this.state;

    return (
      <div className="container" style={{ backgroundColor: "#c8dee4",padding:'5px' }}>
        <div className="row">
          <div className="col s8 offset-s2">
            {/* <Link to="/" className="btn-flat waves-effect">
              <i className="material-icons left">keyboard_backspace</i> Back to
              home
            </Link> */}
            <div className="col s12" style={{ paddingLeft: "3px" }}>
              <h5>
                <b>Register below</b> 
              </h5>
              <p style={{color:'black'}}>
                Already have an account? <b><Link to="/login">Log in</Link></b>
              </p>
            </div>
            <form noValidate onSubmit={this.onSubmit}>
              <div className="input-field col s12">
                <input
                  onChange={this.onChange}
                  value={this.state.name}
                  error={errors.name}
                  id="name"
                  type="text"
                  className={classnames("", {
                    invalid: errors.name,
                  })}
                />
                <label htmlFor="name">Name</label>
                <span className="red-text">{errors.name}</span>
              </div>
             
              <div className="input-field col s12">
                <input
                  onChange={this.onChange}
                  value={this.state.email}
                  error={errors.email}
                  id="email"
                  type="email"
                  className={classnames("", {
                    invalid: errors.email,
                  })}
                />
                <label htmlFor="email">Email</label>
                <span className="red-text">{errors.email}</span>
              </div>
              <div className="input-field col s12">
                <input
                  onChange={this.onChange}
                  value={this.state.password}
                  error={errors.password}
                  id="password"
                  type="password"
                  className={classnames("", {
                    invalid: errors.password,
                  })}
                />
                <label htmlFor="password">Password</label>
                <span className="red-text">{errors.password}</span>
              </div>
              <div className="input-field col s12">
                <input
                  onChange={this.onChange}
                  value={this.state.password2}
                  error={errors.password2}
                  id="password2"
                  type="password"
                  className={classnames("", {
                    invalid: errors.password2,
                  })}
                />
                <label htmlFor="password2">Confirm Password</label>
                <span className="red-text">{errors.password2}</span>
              </div>
              {/* <div className="input-field col s12">
                <input
                  onChange={this.onChange}
                  value={this.state.imageName}
                  error={errors.imageName}
                  id="imageName"
                  type="text"
                  className={classnames("", {
                    invalid: errors.imageName,
                  })}
                />
                <label htmlFor="imageName">Image Name</label>
                <span className="red-text">{errors.imageName}</span>
              </div> */}


              <div className="input-field col s12">
                <input
                  onChange={this.onChange}
                  value={this.state.image}
                  error={errors.image}
                  id="image"
                  type="file"
                  accept="image/png, image/jpeg"
                  className={classnames("", {
                    invalid: errors.image,
                  })}
                />
                {/* <label htmlFor="image">Upload Image </label> */}
                <span className="red-text">{errors.image}</span>
              </div>


              {/* <div className="input-field col s12">
                <input
                 onChange={this.onChange}
                 value={this.state.image}
                  type="file"
                  id="image"
                  name="image"
                  accept="image/png, image/jpeg"
                  className={classnames("", {
                    invalid: errors.image,
                  })}
                />
              </div> */}
              <div className="col s12" style={{ paddingLeft: "11.250px" }}>
                <button
                  style={{
                    width: "90px",
                    height:"40px",
                    padding:"5px",
                    letterSpacing: "1.5px",
                    color:'white',
                    fontWeight:'500',  
                    backgroundColor:'black' ,
                    fontFamily:'Arial'      
                  }}
                  type="submit"
                  // className="btn btn-large waves-effect waves-light hoverable blue accent-3"
                >
                  SIGN UP
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

Register.propTypes = {
  registerUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  auth: state.auth,
  errors: state.errors,
});

export default connect(mapStateToProps, { registerUser })(withRouter(Register));
