import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { logoutUser } from "../../actions/authActions";

class Dashboard extends Component {
  onLogoutClick = (e) => {
    e.preventDefault();
    this.props.logoutUser();
  };

  render() {
    const { user } = this.props.auth;
    console.log(user);
    return (
      <div style={{ height: "75vh" }} className="container valign-wrapper">
        <div className="row">
          <div className="landing-copy col s12 center-align">
            <h4>
              <b>Welcome,</b> {user.name}

            </h4>
            <img
              width="300"
              height="300"
              src={
                user.image
                  ? user.image
                  
                  :"https://developer.mozilla.org/static/img/favicon144.png"
                  
              }
              alt="My Image"
            ></img>
       
            <br></br>
            <button
              style={{
                width: "80px",
                height:"40px",
                padding:"5px",
                letterSpacing: "1.5px",
                color:'white',
                fontWeight:'500',  
                backgroundColor:'black'       
              }}
              onClick={this.onLogoutClick}
              // className="btn btn-large waves-effect waves-light hoverable blue accent-3"
            >
              Logout
            </button>
          </div>
        </div>
      </div>
    );
  }
}

Dashboard.propTypes = {
  logoutUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  auth: state.auth,
});

export default connect(mapStateToProps, { logoutUser })(Dashboard);
